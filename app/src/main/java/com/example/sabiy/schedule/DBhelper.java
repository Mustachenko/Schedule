package com.example.sabiy.schedule;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBhelper extends SQLiteOpenHelper {
    private static int VERSION = 4;
    private static String name = "MySchedule";
    public String cardTableName = "car1dTable";
    public String rowTableName = "rowTable";

    public DBhelper(Context context) {
        super(context, name, null, VERSION);
    }

    public void setCardTableName(String tableName) {
        this.cardTableName = tableName;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        initDB(sqLiteDatabase, 0, VERSION);
        createNewTable(sqLiteDatabase, 0, VERSION);
        exampleScheduleCard(sqLiteDatabase, "Monday", 1, R.color.card_yellow, 4, 4);
    }

    private void createNewTable(SQLiteDatabase database, int oldVersion, int newVersion) {
        if(oldVersion < 2) {
            database.execSQL("CREATE TABLE " + cardTableName + " (CARD_NUMBER INTEGER PRIMARY KEY," +
                    "COLOR TEXT NOT NULL, DAY TEXT NOT NULL, WEEK_NUMBER INTEGER)");
            ContentValues values = new ContentValues();
            database.execSQL("CREATE TABLE " + rowTableName + "(ROW_NUMBER INTEGER NOT NULL, CARD_NUMBER INTEGER NOT NULL, LABLE TEXT NOT NULL, DESCRIPTION TEXT, TIME TEXT, FOREIGN KEY(CARD_NUMBER) REFERENCES " +cardTableName + "(CARD_NUMBER))");
            values.put("TABLE_NAME", cardTableName);
            database.insert("TABLES", null, values);
        }
    }

    private void initDB(SQLiteDatabase database, int oldVersion, int newVersion) {
        if(oldVersion < 2) {
            database.execSQL("CREATE TABLE TABLES  (_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    " TABLE_NAME TEXT NOT NULL)");
        }
    }

    private void exampleScheduleCard(SQLiteDatabase database, String day, int cardNumber, int color, int rowCount, int week){

        for (int i = 0; i < rowCount; i++) {
            ContentValues cardValues = new ContentValues();
            ContentValues rowValues = new ContentValues();
            cardValues.put("DAY", day);
            cardValues.put("CARD_NUMBER", cardNumber);
            cardValues.put("COLOR", color);
            cardValues.put("WEEK_NUMBER", week);
            rowValues.put("CARD_NUMBER", cardNumber);
            rowValues.put("LABLE", "Math" + i);
            rowValues.put("ROW_NUMBER", i);
            rowValues.put("DESCRIPTION", "logic");
            rowValues.put("TIME", "12:30");
            database.insert(cardTableName, null, cardValues );
            database.insert(rowTableName, null, rowValues );
        }
    }

    public int getCardsCount() {
        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor = database.query(cardTableName, new String[] {"COUNT(DISTINCT CARD_NUMBER)"}, null, null, null, null, null);
        if(cursor.moveToFirst()) {
            return cursor.getInt(0);
        }
        else return 0;
    }

    public int getRowCount(int cardNumber) {
        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor = database.query(rowTableName, new String[] {"COUNT(ROW_NUMBER)"}, "CARD_NUMBER=?", new String[] {Integer.toString(cardNumber)}, null, null, null);
        if(cursor.moveToFirst()) {
            return cursor.getInt(0);
        }
        else return 0;
    }


    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        initDB(sqLiteDatabase, 0, VERSION);
        createNewTable(sqLiteDatabase, 0, VERSION);
        exampleScheduleCard(sqLiteDatabase, "Monday", 1, R.color.card_purple, 5, 4);
    }
}
