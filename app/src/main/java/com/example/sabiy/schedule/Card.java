package com.example.sabiy.schedule;

public class Card {
    private int number;
    private String[] lable;
    private String[] description;
    private String[] time;
    private String color;


    public int getNumber() {
        return number;
    }

    public String[] getLable() {
        return lable;
    }

    public String[] getDescription() {
        return description;
    }

    public String[] getTime() {
        return time;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setLable(String[] lable) {
        this.lable = lable;
    }

    public void setDescription(String[] description) {
        this.description = description;
    }

    public void setTime(String[] time) {
        this.time = time;
    }
}
