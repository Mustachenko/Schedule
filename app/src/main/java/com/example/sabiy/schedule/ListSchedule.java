package com.example.sabiy.schedule;


import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;


public class ListSchedule extends Fragment {
    SQLiteDatabase database;
    Cursor cursor;
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.fragment_list_schedule, container ,false);
        ListView listView = (ListView) layout.findViewById(R.id.list_view);
        DBhelper bhelper = new DBhelper(inflater.getContext());
        try {
            database = bhelper.getReadableDatabase();
            cursor = database.query("TABLES", new String[]{"_id", "TABLE_NAME"}, null, null, null, null, null);
            SimpleCursorAdapter adapter = new SimpleCursorAdapter(inflater.getContext(), android.R.layout.simple_list_item_1, cursor, new String[]{"TABLE_NAME"}, new int[]{android.R.id.text1}, 0);
            listView.setAdapter(adapter);
        }catch (SQLException e) {
           Toast.makeText(inflater.getContext(), "Can`t access database", Toast.LENGTH_SHORT).show();
        }
        FloatingActionButton fab = (FloatingActionButton) layout.findViewById(R.id.add_schedule);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(inflater.getContext(), NewSchedule.class);
                inflater.getContext().startActivity(intent);
            }
        });
        return layout;
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        cursor.close();
        database.close();
    }
}
