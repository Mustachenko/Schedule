package com.example.sabiy.schedule.adapters;

import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.sabiy.schedule.DBhelper;
import com.example.sabiy.schedule.R;

import java.util.Calendar;

public class GridListAdapter extends RecyclerView.Adapter<GridListAdapter.GridHolder>{
    private  int rowCount;
    private Context context;
    private int cardNumber;
    private int rowNumber;
    ImageButton deleteField;
    public static final int MAX_COUNT = 7;

    public GridListAdapter(Context context, int rowCount, int cardNumber) {
        this.context = context;
        this.cardNumber = cardNumber;
        this.rowCount = rowCount;
    }

    private void setRowNumber(int i) {
        rowNumber = i;
    }
    public void addField () {
        if(rowCount < MAX_COUNT ){
        rowCount++;
        DBhelper helper = new DBhelper(context);
        SQLiteDatabase database = helper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("ROW_NUMBER", rowCount-1);
        contentValues.put("LABLE", "");
        contentValues.put("DESCRIPTION", "");
        contentValues.put("TIME", "");
        contentValues.put("CARD_NUMBER", cardNumber);
        database.insert(helper.rowTableName, null, contentValues);
        notifyItemInserted(rowCount);
        }
    }

    @NonNull
    @Override
    public GridHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        GridLayout gridLayout = (GridLayout) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.grid_list, viewGroup, false );
        return new GridHolder(gridLayout);
    }

    @Override
    public void onBindViewHolder(@NonNull final GridHolder gridHolder, int i) {
        final GridLayout gridLayout = gridHolder.gridLayout;
        setRowNumber(gridHolder.getAdapterPosition());
        DBhelper helper = new DBhelper(context);
        deleteField = gridLayout.findViewById(R.id.delete_field);
        final int rowNumber = i;
        updateRows(helper, gridLayout, gridHolder);

       gridHolder.time.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Calendar currentTime = Calendar.getInstance();
                int hour = currentTime.get(Calendar.HOUR_OF_DAY);
                int minute = currentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        gridHolder.time.setText( String.format("%02d",selectedHour) + ":" + String.format("%02d", selectedMinute));
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });

        deleteField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int deletedNumber = gridHolder.getAdapterPosition();
                notifyItemRemoved(deletedNumber);
                DBhelper helper = new DBhelper(context);
                SQLiteDatabase database = helper.getWritableDatabase();
                System.out.println("delete" + deletedNumber);
                database.delete(helper.rowTableName, "CARD_NUMBER = " + cardNumber + " AND ROW_NUMBER = " + rowNumber, null);
                rowCount--;

                if(deletedNumber != rowCount) {
                   new UpdateRowIdTask().execute(deletedNumber);
                }
                updateRows(helper, gridLayout, gridHolder);
                notifyDataSetChanged();
                database.close();
            }
        });

    }
    private class UpdateRowIdTask extends AsyncTask<Integer, Void, Boolean> {
        private int deletedNumber;

        @Override
        protected Boolean doInBackground(Integer... deletedNumbers) {
            deletedNumber = deletedNumbers[0];
            int j = deletedNumber;
            try {
                DBhelper helper = new DBhelper(context);
                SQLiteDatabase database = helper.getWritableDatabase();
                for (int i = 1; i <= rowCount; i++) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("ROW_NUMBER", j++);
                    database.update(helper.rowTableName, contentValues, "CARD_NUMBER=? AND ROW_NUMBER=?", new String[]{Integer.toString(cardNumber), Integer.toString(deletedNumber + i)});
                }
                return true;
            } catch (SQLException e) {
                return false;
            }
        }

        protected void OnPostExecute(Boolean success) {
            if(!success) {
                Toast toast = Toast.makeText(context, "Database unavailable", Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    }

    private void updateRows(DBhelper helper, GridLayout gridLayout, GridHolder gridHolder){
        try {
            SQLiteDatabase database = helper.getWritableDatabase();
            Cursor cursor = database.query(helper.rowTableName, new String[]{"ROW_NUMBER", "LABLE", "DESCRIPTION", "TIME"}, "CARD_NUMBER=? AND" +
                    " ROW_NUMBER=?", new String[]{Integer.toString(cardNumber), Integer.toString(rowNumber)}, null, null, null);

           gridHolder.number = (EditText) gridLayout.findViewById(R.id.grid_list_number);
            gridHolder.label = (EditText) gridLayout.findViewById(R.id.grid_list_label);
            gridHolder.time = (TextView) gridLayout.findViewById(R.id.grid_list_time);
            gridHolder.description = (EditText) gridLayout.findViewById(R.id.grid_list_description);
            if(cursor.moveToFirst()) {
                gridHolder.number.setText(cursor.getString(0));
                gridHolder.label.setText(cursor.getString(1));
                gridHolder.time.setText(cursor.getString(3));
                gridHolder.description.setText(cursor.getString(2));
                cursor.close();
            }
        }catch (SQLException e) {
            Toast.makeText(context, "database error Grid Adapter", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public int getItemCount() {
        return rowCount;
    }

    public static class GridHolder extends RecyclerView.ViewHolder {
        private GridLayout gridLayout;
        private EditText number;
        private EditText label;
        private TextView time;
        private EditText description;

        public GridHolder(@NonNull GridLayout itemView) {
            super(itemView);
            gridLayout = itemView;
        }
    }
}
