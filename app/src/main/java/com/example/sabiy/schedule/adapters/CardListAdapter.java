package com.example.sabiy.schedule.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sabiy.schedule.DBhelper;
import com.example.sabiy.schedule.EditCardActivity;
import com.example.sabiy.schedule.R;

public class CardListAdapter extends RecyclerView.Adapter<CardListAdapter.CardHolder> {
    private int days;
    private Context context;

    public CardListAdapter(int  days, Context context) {
        this.days = days;
        this.context = context;
    }

    @NonNull
    @Override
    public CardHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        CardView cardView = (CardView) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cards_list, viewGroup, false);
        return new CardHolder(cardView);
    }

    private void updateList(CardHolder cardHolder, CardView cardView, DBhelper helper) {
            try {
                SQLiteDatabase database = helper.getReadableDatabase();
                Cursor cursor = database.query(helper.rowTableName, new String[]{"ROW_NUMBER", "LABLE", "DESCRIPTION", "TIME"}, "CARD_NUMBER=?", new String[]{Integer.toString(cardHolder.getAdapterPosition() + 1)}, null, null, null);
                Cursor cardCursor = database.query(helper.cardTableName, new String[]{"DAY", "COLOR", "WEEK_NUMBER"}, "CARD_NUMBER=?", new String[]{Integer.toString(cardHolder.getAdapterPosition() + 1)}, null, null, null);
                TextView day = (TextView) cardView.findViewById(R.id.day);
                TextView week = (TextView) cardView.findViewById(R.id.week_number);
                GridLayout table = (GridLayout) cardView.findViewById(R.id.table);


                if (cardCursor.moveToFirst()) {
                    table.setBackgroundResource(cardCursor.getInt(1));
                    day.setText(cardCursor.getString(0));
                    week.setText(cardCursor.getString(2));
                    if(week.getText() != "") {
                        week.setBackgroundResource(R.drawable.white_text_frame);
                    }
                }

                if (cursor.moveToFirst()) {
                    while (!cursor.isAfterLast()) {
                        TextView n = new TextView(cardView.getContext());
                        TextView time = new TextView(cardView.getContext());
                        TextView lable = new TextView(cardView.getContext());
                        TextView description = new TextView(cardView.getContext());

                        n.setBackgroundResource(R.color.text_color);
                        lable.setBackgroundResource(R.color.text_color);
                        time.setBackgroundResource(R.color.text_color);
                        description.setBackgroundResource(R.color.text_color);

                        int margin = (int) cardView.getResources().getDimension(R.dimen.table_text_margin);

                        GridLayout.LayoutParams nParams = new GridLayout.LayoutParams();
                        nParams.rowSpec = GridLayout.spec(GridLayout.UNDEFINED, 2);
                        nParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                        nParams.width = (int) cardView.getResources().getDimension(R.dimen.n_size);
                        n.setLayoutParams(nParams);
                        nParams.setGravity(Gravity.FILL_VERTICAL);
                        nParams.setMargins(margin, margin, margin, margin);
                        n.setGravity(Gravity.CENTER);
                        n.setTextColor(cardView.getResources().getColor(R.color.primary_text_color));

                        GridLayout.LayoutParams lableParams = new GridLayout.LayoutParams();
                        lableParams.width = (int) cardView.getResources().getDimension(R.dimen.lable_width);
                        lableParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                        lableParams.leftMargin = margin;
                        lableParams.rightMargin = margin;
                        lableParams.topMargin = margin;
                        lable.setLayoutParams(lableParams);
                        lable.setGravity(Gravity.CENTER);
                        lable.setTextColor(cardView.getResources().getColor(R.color.primary_text_color));

                        GridLayout.LayoutParams descParams = new GridLayout.LayoutParams();
                        descParams.width = (int) cardView.getResources().getDimension(R.dimen.lable_width);
                        descParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                        descParams.leftMargin = margin;
                        descParams.rightMargin = margin;
                        descParams.bottomMargin = margin;
                        description.setLayoutParams(descParams);
                        description.setGravity(Gravity.CENTER);

                        GridLayout.LayoutParams timeParams = new GridLayout.LayoutParams();
                        timeParams.rowSpec = GridLayout.spec(0, 2);
                        timeParams.width = (int) cardView.getResources().getDimension(R.dimen.time_width);
                        timeParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                        timeParams.setGravity(Gravity.FILL_VERTICAL);
                        timeParams.setMargins(margin, margin, margin, margin);
                        time.setLayoutParams(timeParams);
                        time.setGravity(Gravity.CENTER);
                        time.setTextColor(cardView.getResources().getColor(R.color.primary_text_color));

                        n.setText(cursor.getString(0));
                        lable.setText(cursor.getString(1));
                        time.setText(cursor.getString(3));
                        description.setText(cursor.getString(2));
                        table.addView(n);
                        table.addView(lable);
                        table.addView(time);
                        table.addView(description);
                        if (cursor.isLast()) {
                            n.setBackgroundResource(R.drawable.rounded_corner_bottom_left);
                            time.setBackgroundResource(R.drawable.rounded_corner_bottom_right);
                        }
                        cursor.moveToNext();
                    }
                }
                cursor.close();
                database.close();
            } catch (SQLException e) {
                Toast.makeText(cardView.getContext(), "Database error Card Adapter", Toast.LENGTH_LONG).show();
            }

    }

    @Override
    public void onBindViewHolder(@NonNull final CardHolder cardHolder, int i) {
        final CardView cardView = cardHolder.cardView;
        ImageButton edit = (ImageButton) cardView.findViewById(R.id.edit_card);
        ImageButton delete = (ImageButton) cardView.findViewById(R.id.delete_card);
        final DBhelper dBhelper = new DBhelper(cardView.getContext());
        updateList(cardHolder, cardView, dBhelper);

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(cardView.getContext(), EditCardActivity.class);
                intent.putExtra(EditCardActivity.EXTRA_CARD_NUMBER, cardHolder.getAdapterPosition() + 1);
                context.startActivity(intent);

            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int position) {
                        switch (position){
                            case DialogInterface.BUTTON_POSITIVE:
                                notifyItemRemoved(cardHolder.getAdapterPosition());
                                days--;
                                SQLiteDatabase database = dBhelper.getWritableDatabase();
                                database.delete(dBhelper.cardTableName, "CARD_NUMBER = " + cardHolder.getAdapterPosition() + 1, null);
                                database.delete(dBhelper.rowTableName, "CARD_NUMBER = " + cardHolder.getAdapterPosition() + 1, null);
                                break;
                            case DialogInterface.BUTTON_NEGATIVE: dialogInterface.dismiss();
                                break;
                        }

                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();

            }
        });
    }


    @Override
    public int getItemCount() {
        return days;
    }

    public static class CardHolder extends RecyclerView.ViewHolder {
        private CardView cardView;

        public CardHolder(CardView itemView) {
           super(itemView);
           cardView = itemView;
        }
   }
}