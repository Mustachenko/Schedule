package com.example.sabiy.schedule;

import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableContainer;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.sabiy.schedule.adapters.GridListAdapter;

import java.sql.SQLException;
import java.util.Calendar;

public class EditCardActivity extends AppCompatActivity {
    private int cardNumber;
    public static final String EXTRA_CARD_NUMBER= "cardNumber";
    private final DBhelper bhelper = new DBhelper(this);
    private EditText dayText;
    TextView week;
    private RecyclerView recyclerView;
    GridListAdapter adapter;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        setContentView(R.layout.activity_edit_card);

        cardNumber = getIntent().getExtras().getInt(EXTRA_CARD_NUMBER);
        dayText = findViewById(R.id.day);
        week = (TextView) findViewById(R.id.edit_week_number);
        recyclerView = (RecyclerView) findViewById(R.id.edit_card_table);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter = new GridListAdapter(this, bhelper.getRowCount(cardNumber), cardNumber);

        recyclerView.setAdapter(adapter);

        View [] views = new View[7];
        views[0] = findViewById(R.id.purple_color);
        views[1] = findViewById(R.id.indigo_color);
        views[2] = findViewById(R.id.blue_color);
        views[3] = findViewById(R.id.green_color);
        views[4] = findViewById(R.id.yellow_color);
        views[5] = findViewById(R.id.orange_color);
        views[6] = findViewById(R.id.red_color);

        setOnViewClickListener(views);

        FloatingActionButton fab = findViewById(R.id.fab);
        FloatingActionButton addFab = findViewById(R.id.add_field_fab);

        final SQLiteDatabase database = bhelper.getWritableDatabase();
        Cursor cursor = database.query( bhelper.cardTableName,  new String[]{"DAY", "WEEK_NUMBER"}, "CARD_NUMBER=?", new String[]{Integer.toString(cardNumber)}, null, null, null);
        if(cursor.moveToFirst()){
            dayText.setText(cursor.getString(0));
            week.setText(cursor.getString(1));
            if(week.getText() != "") {
                week.setBackgroundResource(R.drawable.white_text_frame);
            }
        }
        cursor.close();

        week.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                week.startAnimation(AnimationUtils.loadAnimation(EditCardActivity.this, R.anim.scale_color));
                weekNumberPickerListener();
            }
        });

        addFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adapter.addField();
            }
        });
        //TODO write down not only visible items
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String day = dayText.getText().toString();
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("DAY", day);
                        database.update(bhelper.cardTableName, contentValues, "CARD_NUMBER = " + cardNumber, null);
                View v = getCurrentFocus();
                if (v != null) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
               int count = 6;
                ContentValues values = new ContentValues();
                for (int i = 0; i < count; i++) {

                    RecyclerView.ViewHolder holder = (RecyclerView.ViewHolder)
                            recyclerView.findViewHolderForAdapterPosition(i);
                    if (null != holder) {

                        GridLayout layout = (GridLayout)  holder.itemView.findViewById(R.id.grid);
                        layout.setVisibility(View.VISIBLE);
                        EditText number = layout.findViewById(R.id.grid_list_number);
                        EditText label = layout.findViewById(R.id.grid_list_label);
                        System.out.println(label.getText().toString());
                        EditText description = layout.findViewById(R.id.grid_list_description);
                        TextView time = layout.findViewById(R.id.grid_list_time);
                        values.put("LABLE", label.getText().toString());
                        values.put("DESCRIPTION", description.getText().toString());
                        values.put("TIME", time.getText().toString());
                        values.put("CARD_NUMBER", cardNumber);
                        values.put("ROW_NUMBER", i);
                        database.update(bhelper.rowTableName, values, "CARD_NUMBER=? AND ROW_NUMBER=?", new String[]{Integer.toString(cardNumber), Integer.toString(i)});
                    }
                }
            finish();
            }
        });

    }





    private void setOnViewClickListener(final View[] views) {
        int colors[] = new int[7];
        colors[0] = R.color.card_purple;
        colors[1] = R.color.card_indigo;
        colors[2] = R.color.card_blue;
        colors[3] = R.color.card_green;
        colors[4] = R.color.card_yellow;
        colors[5] = R.color.card_orange;
        colors[6] = R.color.card_red;
        for(int i = 0; i < views.length; i++) {
            final int color = colors[i];
            views[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view){
                    view.startAnimation(AnimationUtils.loadAnimation(EditCardActivity.this, R.anim.scale_color));
                    for (int i = 0; i < recyclerView.getChildCount(); i++) {
                        GridLayout layout = (GridLayout) recyclerView.getLayoutManager().getChildAt(i);
                        layout.setBackgroundResource(color);
                    }
                    SQLiteDatabase database = bhelper.getWritableDatabase();
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("COLOR", color);
                    database.update(bhelper.cardTableName, contentValues, "CARD_NUMBER = " + cardNumber, null);
                }
            });
        }
    }

    private void weekNumberPickerListener() {
        final NumberPicker numberPicker = new NumberPicker(this);
        WindowManager.LayoutParams dialogParams = new WindowManager.LayoutParams();
        WindowManager.LayoutParams pickerParams = new WindowManager.LayoutParams();
        pickerParams.width = (int) getResources().getDimension(R.dimen.picker_width);
        pickerParams.height = (int) getResources().getDimension(R.dimen.height);
        numberPicker.startAnimation(AnimationUtils.loadAnimation(this, R.anim.scale_dialog_button_animation));
        dialogParams.gravity = Gravity.LEFT;
        dialogParams.width = (int) getResources().getDimension(R.dimen.picker_width);
        dialogParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialogParams.x = 8;
        numberPicker.setMaxValue(4);
        numberPicker.setMinValue(1);
        numberPicker.setLayoutParams(dialogParams);
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppTheme_PopupOverlay);
        builder.setView(numberPicker);
        builder.setCancelable(true);
        builder.setPositiveButton(" ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String weekNum = Integer.toString(numberPicker.getValue());
                week.setText(weekNum);
                SQLiteDatabase database = bhelper.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put("WEEK_NUMBER", weekNum);
                database.update(bhelper.cardTableName, values, "CARD_NUMBER=?", new String[]{Integer.toString(cardNumber)});
            }
        });
        AlertDialog dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.show();

        Button button = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        button.startAnimation(AnimationUtils.loadAnimation(this, R.anim.scale_dialog_button_animation));
        LinearLayout.LayoutParams buttonParams = (LinearLayout.LayoutParams) button.getLayoutParams();
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        buttonParams.width = (int) getResources().getDimension(R.dimen.picker_button_size);
        buttonParams.height = (int) getResources().getDimension(R.dimen.picker_button_size);

        buttonParams.topMargin = 10;
        button.setLayoutParams(buttonParams);
        button.setBackgroundResource(R.drawable.circle_green);

        dialog.getWindow().setAttributes(dialogParams);

    }
}
